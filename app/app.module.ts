import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddmeteoService } from './addmeteo.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, FormsModule,    ReactiveFormsModule, HttpClientModule


  ],
  providers: [AddmeteoService
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }
