import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AddmeteoService } from './addmeteo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  location: any;
  ville = new FormControl('');
  resultat: any;

     constructor(private meteoserv : AddmeteoService) { }

  onClickSubmit() {
     console.log('la ville saisie est : ' + this.ville.value);
     this.location=this.ville.value;
     console.log(this.location);
     this.meteoserv.getWeather(this.location)
.subscribe(
  res => {
    console.log(res);
   this.resultat=res;
console.log(this.resultat.request);
console.log(this.resultat.location )
console.log(this.resultat.current )

  },
  err => {
    console.log ("erreur");
  }
)

  }

}
  