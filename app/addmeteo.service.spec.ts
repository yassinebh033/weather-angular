import { TestBed } from '@angular/core/testing';

import { AddmeteoService } from './addmeteo.service';

describe('AddmeteoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddmeteoService = TestBed.get(AddmeteoService);
    expect(service).toBeTruthy();
  });
});
